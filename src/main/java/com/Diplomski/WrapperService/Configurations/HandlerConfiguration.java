package com.Diplomski.WrapperService.Configurations;

import com.Diplomski.WrapperService.Handlers.Contracts.HandlerTypes;
import com.Diplomski.WrapperService.Handlers.Contracts.IHandler;
import com.Diplomski.WrapperService.Handlers.Contracts.IHandlerFactory;
import com.Diplomski.WrapperService.Handlers.Exceptions.HandlerNotFoundException;
import com.Diplomski.WrapperService.Handlers.Factory.HandlerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HandlerConfiguration {

    @Value("${handler.type}")
    private HandlerTypes handlerType;

    @Bean
    public IHandlerFactory handlerFactory(@Value("${codeFile.location}") String userCodePath) {
        return new HandlerFactory(userCodePath);
    }

    @Bean
    public IHandler handler(IHandlerFactory handlerFactory) throws HandlerNotFoundException {
        return handlerFactory.getHandler(handlerType);
    }

}
